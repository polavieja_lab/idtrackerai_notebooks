# idtracker.ai notebooks

This repository includes IPython notebooks that we created for different courses to explain and use [idtracker.ai](idtracker.ai).

The folder *deep_learning* migrated to a specific [Machine Learning repository](https://gitlab.com/polavieja_lab/ML_notebooks/) including notebooks to explain some basics on machine learning, neural networks and convolutional neural networks. The *miniCARP* dataset is still there not to brake other scripts that depend on them, but they migrated to [Google Drive](https://drive.google.com/drive/folders/1hThve3rLZKGMmvBZf8VYIqh_FqMhOta1).

The folder *trajectories_analysis* includes notebooks on how to perform different types of analysis on the trajectories obtained with [idtracker.ai](idtracker.ai). These notebooks are based on [trajectorytools](https://gitlab.com/polavieja_lab/trajectorytools).

The folder *idtrackerai_objects_analysis* includes a notebook to analyze fish midlines with [this GitLab repository](https://gitlab.com/polavieja_lab/midline)

The folder *data* includes a trajectory file needed to run the different notebooks.

If you use any code from this repository please reference the article.

**[1] Romero-Ferrero, F., Bergomi, M.G., Hinz, R.C., Heras, F.J.H., de Polavieja, G.G., Nature Methods, 2019. idtracker.ai: tracking all individuals in small or large collectives of unmarked animals.**
